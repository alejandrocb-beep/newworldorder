// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyCRfVLF0ae8ZK6za6WpXTF8DF4iPFJJh-A",
  authDomain: "newwoldorderm7.firebaseapp.com",
  databaseURL: "https://newwoldorderm7-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "newwoldorderm7",
  storageBucket: "newwoldorderm7.appspot.com",
  messagingSenderId: "510894282914",
  appId: "1:510894282914:web:372cd5c0fc1496dc939ffb",
  measurementId: "G-P7H1KBCCXK"
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
