import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { Data, NavigationExtras, Router } from '@angular/router';

import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';


import { UsersService } from '../services/users.service';
import {GlobalService} from '../global.service';

export interface users {

  nom: string;
  correu: string;
  rol: string;
  diners: number;
}


var provider = new firebase.auth.GoogleAuthProvider();

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage implements OnInit {

  ishidden = false;
  newUser: users;
  sdfsd: any;
  userList = [];
  glitched = false;

  constructor(  private usersService: UsersService, private afAuth: AngularFireAuth, private route: Router, public modalController: ModalController,  public global: GlobalService) {
    this.newUser = {} as users;

  }

  loggear(){
    this.global.log();
    this.logged = this.global.logger;
  }

  ngOnInit() {
    this.presentModal();
    this.usersService.read_user().subscribe(data => {

      this.userList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          correu: e.payload.doc.data()['correu'],
          rol: e.payload.doc.data()['rol'],
          diners: e.payload.doc.data()['diners']
        };
      })
      console.log(this.userList);

    });
  }


  userData: users;

  CreateRecord(user: users) {
    console.log(this.userData);
    this.usersService.create_user(user).then(resp => {
      this.sdfsd.reset();
    })
      .catch(error => {
        console.log(error);
      });
  }

  

  picture;
  name = "Encara no has fet log-in. Inicia sessió.";
  diners = 0;
  email = "";
  logged = this.global.logger;
  ADMIN = false;
  showMenu = false;

  navigationExtras: NavigationExtras = { state: {nom: this.name } };

  toggle(){
    console.log("pickar"+this.ishidden);
    if (this.ishidden == true){
      this.ishidden = false;
    } else {
      this.ishidden = true;
    }
  }

  async loginGoogle() {
    const res = await this.afAuth.signInWithPopup(provider);
    const user = res.user;
    console.log(user);
    this.picture = user.photoURL;
    this.name = user.displayName;
    this.email = user.email;
    this.loggear();
    console.log(this.logged)
    let nou = true;
    let goAdmin = false;
    for (let entry of this.userList){
      this.diners = entry.diners;
      if (entry.rol === "ADMIN" && this.email == entry.correu){
        goAdmin = true;
        break;
      }else if (this.email === entry.correu){
        nou = false;
        break;
      }
    }
    
    if (goAdmin){
      nou = false;
      this.goAdmin();
    }

    if (nou){
      console.log("porque entra");
      this.newUser.nom =  user.displayName;
      this.newUser.correu = user.email;
      this.newUser.rol = "client";
      this.newUser.diners = 0;
      this.CreateRecord(this.newUser);
    }
 }

 async tancarGoogle() {
  const res = await this.afAuth.signOut;
  this.picture = 'https://www.simplifai.ai/wp-content/uploads/2019/06/blank-profile-picture-973460_960_720-400x400.png';
  this.name = "Encara no has fet log-in. Inicia sessió.";
  this.email = "";
  this.diners = 0;
  this.logged = false;
  this.ADMIN = false;
}

async showTheMenu(){
  this.glitched = true;
  await this.delay(250);
  this.glitched = false;
  console.log("pickar"+this.showMenu)
  if (this.showMenu == true){
    this.showMenu = false;
  } else {
    this.showMenu = true;
  }
}



  async goBorsa() {
  this.glitched = true;
  await this.delay(250);
  this.glitched = false;
  this.navigationExtras.state.nom = this.name;
  this.navigationExtras.state.saldo = this.diners;
  this.navigationExtras.state.correu = this.email;
  for (let entry of this.userList){
    console.log("mira todo")
    if (entry.correu === this.email){
      this.navigationExtras.state.id = entry.id;
      this.navigationExtras.state.rol = entry.rol;
    }
  }
  if (this.logged){
    this.route.navigate(['./borsa'], this.navigationExtras);
  } else {
    console.log("ACCESS DENIED to BORSA!")
  }
}
  async goDefensa() {
  this.glitched = true;
  await this.delay(250);
  this.glitched = false;
  this.navigationExtras.state.nom = this.name;
  this.navigationExtras.state.saldo = this.diners;
  this.navigationExtras.state.correu = this.email;
  for (let entry of this.userList){
    console.log("mira todo")
    if (entry.correu === this.email){
      this.navigationExtras.state.id = entry.id;
      this.navigationExtras.state.rol = entry.rol;
    }
  }
  if (this.logged){
    this.route.navigate(['./defensa'], this.navigationExtras);
  } else {
    console.log("ACCESS DENIED to DEFENSA!")
  }
}
  async goEspai() {
  this.glitched = true;
  await this.delay(250);
  this.glitched = false;
  this.navigationExtras.state.nom = this.name;
  this.navigationExtras.state.saldo = this.diners;
  this.navigationExtras.state.correu = this.email;
  for (let entry of this.userList){
    console.log("mira todo")
    if (entry.correu === this.email){
      this.navigationExtras.state.id = entry.id;
      this.navigationExtras.state.rol = entry.rol;
    }
  }
  if (this.logged){
    this.route.navigate(['./espai'], this.navigationExtras);
  } else {
    console.log("ACCESS DENIED to ESPAI!")
  }
}

async goAdmin() {
  this.glitched = true;
  await this.delay(250);
  this.glitched = false;
  this.navigationExtras.state.nom = this.name;
  this.navigationExtras.state.saldo = this.diners;
  this.navigationExtras.state.correu = this.email;
  for (let entry of this.userList){
    console.log("mira todo")
    if (entry.correu === this.email){
      this.navigationExtras.state.id = entry.id;
      this.navigationExtras.state.rol = "ADMIN";
    }
  }
  if (this.logged){
    this.route.navigate(['./admin'], this.navigationExtras);
  } else {
    console.log("ACCESS DENIED to ADMIN!")
  }
}


async presentModal() {
  const modal = await this.modalController.create({
    component: ModalPage,
    cssClass: 'my-custom-modal-css'
  });
  return await modal.present();
}

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

}
