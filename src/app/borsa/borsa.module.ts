import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BorsaPageRoutingModule } from './borsa-routing.module';

import { BorsaPage } from './borsa.page';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    BorsaPageRoutingModule
  ],
  declarations: [BorsaPage]
})
export class BorsaPageModule {}
