import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BorsaPage } from './borsa.page';

const routes: Routes = [
  {
    path: '',
    component: BorsaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BorsaPageRoutingModule {}
