import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TreballService } from './../shared/treball.service';
import { Treball } from './../shared/Treball';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';

import { users } from '../home/home.page';

@Component({
  selector: 'app-borsa',
  templateUrl: './borsa.page.html',
  styleUrls: ['./borsa.page.scss'],
})
export class BorsaPage implements OnInit {

  logged = true;

  notLogged = true;

  menuOFF = true;

  correu: string;

  glitched = false;

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  async showTheMenu(){
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    console.log("pickar"+this.menuOFF)
    if (this.menuOFF == true){
      this.menuOFF = false;
    } else {
      this.menuOFF = true;
    }
  }

  

  
  saldo: number;
  cargado = false;
  cargando = false;
  job1c = false;
  job2c = false;
  studentList = [];
  job1 = Treball;
  job2 = Treball;
  studentData: Treball;
  studentForm: FormGroup;
  sub: any;
  nombre = "";
  rol: string;
  id: string;

  userData: users;


  constructor(
    private firebaseService: FirebaseService,
    private route: ActivatedRoute,
    private aptService: TreballService,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.studentData = {} as Treball;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.nombre= this.router.getCurrentNavigation().extras.state.nom;
        this.saldo = this.router.getCurrentNavigation().extras.state.saldo;
        this.correu = this.router.getCurrentNavigation().extras.state.correu;
        this.id = this.router.getCurrentNavigation().extras.state.id;
        this.rol = this.router.getCurrentNavigation().extras.state.rol;
      }
    });
  }
  
  userList = [];
  navigationExtras: NavigationExtras = { state: { } };

  async goHome() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    if (this.logged){
      this.router.navigate(['./home']);
    } else {
      console.log("ACCESS DENIED to HOME!")
    }
  }

  async getJob(subir: string){
    if (this.cargado == true){
      this.cargado = false;
    }
    this.cargando = true;
    await this.delay(1000);
    let record = {};
    record['correu'] = this.correu;
    record['diners'] = this.saldo + parseInt(subir);
    record['nom'] = this.nombre;
    record['rol'] = this.rol;

    this.firebaseService.update(this.id,record,"users");
    this.saldo = this.saldo + parseInt(subir);
    this.cargando = false;
    this.cargado = true;
  }

  async goDefensa() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    this.navigationExtras.state.nom = this.nombre;
    this.navigationExtras.state.saldo = this.saldo;
    this.navigationExtras.state.correu = this.correu;
    for (let entry of this.userList){
      console.log("mira todo")
      if (entry.correu === this.correu){
        this.navigationExtras.state.id = entry.id;
        this.navigationExtras.state.rol = entry.rol;
      }
    }
    if (this.logged){
      this.router.navigate(['./defensa'], this.navigationExtras);
    } else {
      console.log("ACCESS DENIED to DEFENSA!")
    }
  }
  async goEspai() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    this.navigationExtras.state.nom = this.nombre;
    this.navigationExtras.state.saldo = this.saldo;
    this.navigationExtras.state.correu = this.correu;
    for (let entry of this.userList){
      console.log("mira todo")
      if (entry.correu === this.correu){
        this.navigationExtras.state.id = entry.id;
        this.navigationExtras.state.rol = entry.rol;
      }
    }
    if (this.logged){
      this.router.navigate(['./espai'], this.navigationExtras);
    } else {
      console.log("ACCESS DENIED to ESPAI!")
    }
  }



  ngOnInit() {

    this.studentForm = this.fb.group({
      carrec: ['', [Validators.required]],
      descripcio: ['', [Validators.required]],
      numContacte: ['', [Validators.required]],
      email: ['', [Validators.required]],
      horari: ['', [Validators.required]],
      salari: ['', [Validators.required]],
      imageUrl: ['', [Validators.required]],
    })

    this.aptService.read_treball().subscribe(data => {

      this.studentList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          carrec: e.payload.doc.data()['carrec'],
          descripcio: e.payload.doc.data()['descripcio'],
          numContacte: e.payload.doc.data()['numContacte'],
          email: e.payload.doc.data()['email'],
          horari: e.payload.doc.data()['horari'],
          salari: e.payload.doc.data()['salari'],
          imageUrl: e.payload.doc.data()['imageUrl'],
          clicked: false,
          bought: false
        };
      })
      console.log(this.studentList);
    
      this.job1 = this.studentList[7];
      this.job2 = this.studentList[4];
      console.log(this.job1);
    });
    this.notLogged = false;
    if (this.nombre === ""){
      this.notLogged = true;
    }
    console.log(this.notLogged);
  }

}
