import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FerTreballPage } from './fer-treball.page';

const routes: Routes = [
  {
    path: '',
    component: FerTreballPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FerTreballPageRoutingModule {}
