import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TreballService } from './../shared/treball.service';
import { Treball } from './../shared/Treball';

@Component({
  selector: 'app-fer-treball',
  templateUrl: './fer-treball.page.html',
  styleUrls: ['./fer-treball.page.scss'],
})
export class FerTreballPage implements OnInit {
  studentList = [];
  studentData: Treball;
  studentForm: FormGroup;

  constructor(
    private aptService: TreballService,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.studentData = {} as Treball;
  }

  ngOnInit() {

    this.studentForm = this.fb.group({
      carrec: ['', [Validators.required]],
      descripcio: ['', [Validators.required]],
      numContacte: ['', [Validators.required]],
      email: ['', [Validators.required]],
      horari: ['', [Validators.required]],
      salari: ['', [Validators.required]],
      imageUrl: ['', [Validators.required]]
    })

    this.aptService.read_treball().subscribe(data => {

      this.studentList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          carrec: e.payload.doc.data()['carrec'],
          descripcio: e.payload.doc.data()['descripcio'],
          numContacte: e.payload.doc.data()['numContacte'],
          email: e.payload.doc.data()['email'],
          horari: e.payload.doc.data()['horari'],
          salari: e.payload.doc.data()['salari'],
          imageUrl: e.payload.doc.data()['imageUrl']
        };
      })
      console.log(this.studentList);

    });
  }

  CreateRecord() {
    console.log(this.studentForm.value);
    this.aptService.create_treball(this.studentForm.value).then(resp => {
      this.studentForm.reset();
    })
      .catch(error => {
        console.log(error);
      });
  }

  RemoveRecord(rowID) {
    this.aptService.delete_treball(rowID);
  }

  EditRecord(record) {
    record.isEdit = true;
    record.EditCarrec = record.carrec;
    record.EditDesc = record.descripcio;
    record.EditNum = record.numContacte;
    record.EditEmail = record.email;
    record.EditHorari = record.horari;
    record.EditSalari = record.salari;
    record.EditImage = record.imageUrl;
  }

  UpdateRecord(recordRow) {
    let record = {};
    record['carrec'] = recordRow.EditCarrec;
    record['descripcio'] = recordRow.EditDesc;
    record['numContacte'] = recordRow.EditNum;
    record['email'] = recordRow.EditEmail;
    record['horari'] = recordRow.EditHorari;
    record['salari'] = recordRow.EditSalari;
    record['imageUrl'] = recordRow.EditImage;
    this.aptService.update_treball(recordRow.id, record);
    recordRow.isEdit = false;
  }

}