import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FerTreballPageRoutingModule } from './fer-treball-routing.module';

import { FerTreballPage } from './fer-treball.page';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FerTreballPageRoutingModule
  ],
  declarations: [FerTreballPage]
})
export class FerTreballPageModule {}
