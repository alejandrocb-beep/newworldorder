import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  public logger = false;

  constructor() { }

  log(){
    this.logger = true;
  }

}
