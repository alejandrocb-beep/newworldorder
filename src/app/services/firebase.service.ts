// firebase.service.ts
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {


  constructor(
    private firestore: AngularFirestore
  ) { }

  create(record, collectionName) {
    return this.firestore.collection(collectionName).add(record);
  }

  read(collectionName) {
    return this.firestore.collection(collectionName).snapshotChanges();
  }

  update(recordID, record, collectionName) {
    this.firestore.doc(collectionName + '/' + recordID).update(record);
  }

  delete(record_id, collectionName) {
    this.firestore.doc(collectionName + '/' + record_id).delete();
  }
}
