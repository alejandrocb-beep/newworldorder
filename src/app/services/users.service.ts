import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  collectionName = 'users';

  constructor(
    private firestore: AngularFirestore
  ) { }

  create_user(record) {
    return this.firestore.collection(this.collectionName).add(record);
  }

  read_user() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }

}
