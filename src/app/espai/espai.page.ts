import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-espai',
  templateUrl: './espai.page.html',
  styleUrls: ['./espai.page.scss'],
})
export class EspaiPage implements OnInit {

  divsList = [];
  color = 'light';
  notLogged = true;
  imatgeEscollida = "http://i.imgur.com/bcAvaZU.gif";
  nombre = "";
  saldo = 0;
  id = "not logged";
  rol = "not logged";
  constructor(private route: ActivatedRoute,
    private router: Router) { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.nombre= this.router.getCurrentNavigation().extras.state.nom;
          this.saldo = this.router.getCurrentNavigation().extras.state.saldo;
          this.correu = this.router.getCurrentNavigation().extras.state.correu;
          this.id = this.router.getCurrentNavigation().extras.state.id;
          this.rol = this.router.getCurrentNavigation().extras.state.rol;
        }
      });
    }

  carrusel = 0;

  ngOnInit() {
    this.notLogged = false;
    if (this.nombre === ""){
      this.notLogged = true;
    }
  }

  contador: 0;

  customColor = 'dark';

  logged = true;

  menuOFF = true;

  correu: string;

  glitched = false;

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  async showTheMenu(){
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    console.log("pickar"+this.menuOFF)
    if (this.menuOFF == true){
      this.menuOFF = false;
    } else {
      this.menuOFF = true;
    }
  }


  cargado = false;
  cargando = false;

  async goHome() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    if (this.logged){
      this.router.navigate(['./home']);
    } else {
      console.log("ACCESS DENIED to HOME!")
    }
  }


  addDiv(){
    this.contador++;
    this.divsList.push(this.contador);
  }

  removeDiv(num: number){
    let index = this.divsList.indexOf(num);

    this.divsList.splice(index, 1);

  }

  changePhoto(){
    switch(this.carrusel) { 
      case 0: { 
        this.imatgeEscollida = "./assets/Imagenes/espai2.jpg";
        this.carrusel++;
        break; 
      } 
      case 1: { 
        this.imatgeEscollida = "./assets/Imagenes/espai3.jpg";
        this.carrusel++;
        break; 
      }
      case 2: { 
        this.imatgeEscollida = "./assets/Imagenes/espai4.jpg";
        this.carrusel++;
        break; 
     }
     case 3: { 
      this.imatgeEscollida = "./assets/Imagenes/espai5.jpg";
      this.carrusel++;
      break; 
      }
      case 4: { 
        this.imatgeEscollida = "./assets/Imagenes/espai6.jpg";
        this.carrusel++;
        break; 
      }
      default: { 
        this.imatgeEscollida = "./assets/Imagenes/espai.jpg";
        this.carrusel = 0;
        break; 
      } 
   } 
  }

  changeColor(){
    switch(this.color) { 
      case 'white': { 
        this.color = 'pink';
        break; 
      } 
      case 'pink': { 
        this.color = 'blue';
        break; 
      } 
      case 'blue': { 
        this.color = 'green';
        break; 
      } 
      case 'green': { 
        this.color = 'yellow';
        break; 
      } 
      case 'yellow': { 
        this.color = 'black';
        break; 
      } 
      default: { 
        this.color = 'white';
        break; 
      } 
   } 
  }

  changeNotepad(){
    switch(this.customColor) { 
      case 'dark': { 
        this.customColor = 'light';
        break; 
      } 
      case 'light': { 
        this.customColor = 'success';
        break; 
      } 
      case 'success': { 
        this.customColor = 'danger';
        break; 
      } 
      case 'danger': { 
        this.customColor = 'primary';
        break; 
      } 
      case 'primary': { 
        this.customColor = 'warning';
        break; 
      } 
      default: { 
        this.customColor = 'dark';
        break; 
      } 
   } 
  }

}
