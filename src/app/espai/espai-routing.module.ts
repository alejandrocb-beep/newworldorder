import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EspaiPage } from './espai.page';

const routes: Routes = [
  {
    path: '',
    component: EspaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EspaiPageRoutingModule {}
