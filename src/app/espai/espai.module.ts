import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EspaiPageRoutingModule } from './espai-routing.module';

import { EspaiPage } from './espai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EspaiPageRoutingModule
  ],
  declarations: [EspaiPage]
})
export class EspaiPageModule {}
