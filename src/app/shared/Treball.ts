export class Treball {
    $key: string;
    carrec: string;
    descripcio: string;
    numContacte: number;
    email: string;
    hores: string;
    salari: number;
    imageUrl: string;
    clicked: boolean;
}