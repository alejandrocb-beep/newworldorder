import { TestBed } from '@angular/core/testing';

import { TreballService } from './treball.service';

describe('TreballService', () => {
  let service: TreballService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TreballService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
