import { Injectable } from '@angular/core';
import { Treball } from '../shared/Treball';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TreballService {

  collectionName = 'treball';

  constructor(private firestore: AngularFirestore) { }

create_treball(record) {
    return this.firestore.collection(this.collectionName).add(record);
  }

  read_treball() {
    return this.firestore.collection(this.collectionName).snapshotChanges();
  }

  update_treball(recordID, record) {
    this.firestore.doc(this.collectionName + '/' + recordID).update(record);
  }

  delete_treball(record_id) {
    this.firestore.doc(this.collectionName + '/' + record_id).delete();
  }

}
