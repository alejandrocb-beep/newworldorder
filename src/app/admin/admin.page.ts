import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { FerTreballPage } from '../fer-treball/fer-treball.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

export class Comunitat {

  constructor(nombre: string, dinero: number, funciona: boolean) { 
    this.nom = nombre;
    this.saldo = dinero;
    this.activa = funciona;
  }

  nom: string;
  saldo: number;
  activa: boolean;
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})




export class AdminPage implements OnInit {

  constructor( private route: ActivatedRoute, private router: Router,private usersService: UsersService, public modalController: ModalController) { 

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.nombre= this.router.getCurrentNavigation().extras.state.nom;
      }
    });

    this.euskalGaliza = new Comunitat("Euskal Galiza",10000,true);
    this.illojuan = new Comunitat("Illojuan",10000,true);
    this.madrid = new Comunitat("Madrid",10000,true);
    this.paisosCatalans = new Comunitat("Països Catalans",10000,true);
  }

  notLogged = true;
  nombre = "";
  registrats = 0;
  plovent = false;
  raig = false;
  asalt2=  false;
  euskalGaliza: Comunitat;
  illojuan: Comunitat;
  paisosCatalans: Comunitat;
  madrid: Comunitat;
  webTicker = "WebTicker per defecte...";
  info = "Selecciona alguna opció per mostrar informació";
  info2 = "";
  mapa = "./assets/mapas/mapa-base.png";

  userList = [];

  ngOnInit() {
    this.usersService.read_user().subscribe(data => {

      this.userList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          correu: e.payload.doc.data()['correu'],
          rol: e.payload.doc.data()['rol'],
          diners: e.payload.doc.data()['diners']
        };
      })
      console.log(this.userList);
      this.registrats = this.userList.length;
      this.notLogged = false;
      if (this.nombre === ""){
        this.notLogged = true;
      }
    });
    
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FerTreballPage
    });
    return await modal.present();
  }

  mostrarSaldo(){
    this.plovent = false;
    this.raig = false;
    this.asalt2 = false;
    this.info = "";
    this.info2 = "";
    this.info += this.euskalGaliza.nom+": "+this.euskalGaliza.saldo+"\n";
    this.info += this.madrid.nom+": "+this.madrid.saldo+"\n";
    this.info2 += this.illojuan.nom+": "+this.illojuan.saldo+"\n";
    this.info2+= this.paisosCatalans.nom+": "+this.paisosCatalans.saldo+"\n";
  }

  mostrarEstat(){
    this.plovent = false;
    this.raig = false;
    this.asalt2 = false;
    this.info = "";
    this.info2 = "";
    if (this.euskalGaliza.activa){
      this.info += this.euskalGaliza.nom+": "+"ACTIVA"+"\n";
    } else {
      this.info += this.euskalGaliza.nom+": "+"DESTRUIDA"+"\n";
    }
    if (this.madrid.activa){
      this.info += this.madrid.nom+": "+"ACTIVA"+"\n";
    } else {
      this.info += this.madrid.nom+": "+"DESTRUIDA"+"\n";
    }
    if (this.illojuan.activa){
      this.info2 += this.illojuan.nom+": "+"ACTIVA"+"\n";
    } else {
      this.info2 += this.illojuan.nom+": "+"DESTRUIDA"+"\n";
    }
    if (this.paisosCatalans.activa){
      this.info2 += this.paisosCatalans.nom+": "+"ACTIVA"+"\n";
    } else {
      this.info2 += this.paisosCatalans.nom+": "+"DESTRUIDA"+"\n";
    }
  }

  inundacio(){
    this.plovent = true;
    this.raig = false;
    this.asalt2 = false;
    this.info = "";
    this.info2 = "";
    this.info = " ";

    switch(Math.floor(Math.random() * (3 + 1))) { 
      case 0: {
        this.euskalGaliza.saldo -= 3000;
        if (this.euskalGaliza.saldo >= 0){
          this.info += this.euskalGaliza.nom+" s'ha inundat!";
          this.info2 += this.euskalGaliza.saldo+"\n";
          this.info2 += "-3000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.euskalGaliza.saldo;
        } else {
          this.euskalGaliza.activa = false;
          this.info += this.euskalGaliza.nom +" ha caigut per l'inundació!";
          this.cambiarMapa();
        }
        break; 
      } 
      case 1: {
        this.madrid.saldo -= 3000;
        if (this.madrid.saldo >= 0){
          this.info += this.madrid.nom+" s'ha inundat!";
          this.info2 += this.madrid.saldo+"\n";
          this.info2 += "-3000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.madrid.saldo;
        } else {
          this.madrid.activa = false;
          this.info += this.madrid.nom +" ha caigut per l'inundació!";
          this.cambiarMapa();
        }
        break; 
      }
      case 2: {
        this.illojuan.saldo -= 3000;
        if (this.illojuan.saldo >= 0){
          this.info += this.illojuan.nom+" s'ha inundat!";
          this.info2 += this.illojuan.saldo+"\n";
          this.info2 += "-3000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.illojuan.saldo;
        } else {
          this.illojuan.activa = false;
          this.info += this.illojuan.nom +" ha caigut per l'inundació!";
          this.cambiarMapa();
        }
        break; 
     }
     case 3: {
      this.paisosCatalans.saldo -= 3000;
      if (this.paisosCatalans.saldo >= 0){
        this.info += this.paisosCatalans.nom+" s'ha inundat!";
        this.info2 += this.paisosCatalans.saldo+"\n";
        this.info2 += "-3000"+"\n";
        
        this.info2 += "-----------"+"\n";
        this.info2 += this.paisosCatalans.saldo;
      } else {
        this.paisosCatalans.activa = false;
        this.info += this.paisosCatalans.nom +" ha caigut per l'inundació!";
        this.cambiarMapa();
      }
      break; 
     }
   } 

  }

  asalt(){
    this.plovent = false;
    this.raig = false;
    this.asalt2 = true;
    this.info = "";
    this.info2 = "";
    this.info = " ";

    switch(Math.floor(Math.random() * (3 + 1))) { 
      case 0: {
        this.euskalGaliza.saldo -= 2000;
        if (this.euskalGaliza.saldo >= 0){
          this.info += this.euskalGaliza.nom+" ha sigut asaltada!";
          this.info2 += this.euskalGaliza.saldo+"\n";
          this.info2 += "-2000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.euskalGaliza.saldo;
        } else {
          this.euskalGaliza.activa = false;
          this.info += this.euskalGaliza.nom +" ha caigut per l'asalt!";
          this.cambiarMapa();
        }
        break; 
      } 
      case 1: {
        this.madrid.saldo -= 2000;
        if (this.madrid.saldo >= 0){
          this.info += this.madrid.nom+" ha sigut asaltada!";
          this.info2 += this.madrid.saldo+"\n";
          this.info2 += "-2000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.madrid.saldo;
        } else {
          this.madrid.activa = false;
          this.info += this.madrid.nom +" ha caigut per l'asalt!";
          this.cambiarMapa();
        }
        break; 
      }
      case 2: {
        this.illojuan.saldo -= 2000;
        if (this.illojuan.saldo >= 0){
          this.info += this.illojuan.nom+" ha sigut asaltada!";
          this.info2 += this.illojuan.saldo+"\n";
          this.info2 += "-2000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.illojuan.saldo;
        } else {
          this.illojuan.activa = false;
          this.info += this.illojuan.nom +" ha caigut per l'asalt!";
          this.cambiarMapa();
        }
        break; 
     }
     case 3: {
      this.paisosCatalans.saldo -= 2000;
      if (this.paisosCatalans.saldo >= 0){
        this.info += this.paisosCatalans.nom+" ha sigut asaltada!";
        this.info2 += this.paisosCatalans.saldo+"\n";
        this.info2 += "-2000"+"\n";
        
        this.info2 += "-----------"+"\n";
        this.info2 += this.paisosCatalans.saldo;
      } else {
        this.paisosCatalans.activa = false;
        this.info += this.paisosCatalans.nom +" ha caigut per l'asalt!";
        this.cambiarMapa();
      }
      break; 
     }
   } 

  }

  tempestaElectrica(){
    this.plovent = true;
    this.raig = true;
    this.asalt2 = false;
    this.info = "";
    this.info2 = "";
    this.info = " ";

    switch(Math.floor(Math.random() * (3 + 1))) { 
      case 0: {
        this.euskalGaliza.saldo -= 5000;
        if (this.euskalGaliza.saldo >= 0){
          this.info += this.euskalGaliza.nom+" ha esclatat amb una tempesta elèctrica!";
          this.info2 += this.euskalGaliza.saldo+"\n";
          this.info2 += "-5000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.euskalGaliza.saldo;
        } else {
          this.euskalGaliza.activa = false;
          this.info += this.euskalGaliza.nom +" ha caigut per la tempesta!";
          this.cambiarMapa();
        }
        break; 
      } 
      case 1: {
        this.madrid.saldo -= 5000;
        if (this.madrid.saldo >= 0){
          this.info += this.madrid.nom+" ha esclatat amb una tempesta elèctrica!";
          this.info2 += this.madrid.saldo+"\n";
          this.info2 += "-5000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.madrid.saldo;
        } else {
          this.madrid.activa = false;
          this.info += this.madrid.nom +" ha caigut per la tempesta!";
          this.cambiarMapa();
        }
        break; 
      }
      case 2: {
        this.illojuan.saldo -= 5000;
        if (this.illojuan.saldo >= 0){
          this.info += this.illojuan.nom+" ha esclatat amb una tempesta elèctrica!";
          this.info2 += this.illojuan.saldo+"\n";
          this.info2 += "-5000"+"\n";
          
          this.info2 += "-----------"+"\n";
          this.info2 += this.illojuan.saldo;
        } else {
          this.illojuan.activa = false;
          this.info += this.illojuan.nom +" ha caigut per la tempesta!";
          this.cambiarMapa();
        }
        break; 
     }
     case 3: {
      this.paisosCatalans.saldo -= 5000;
      if (this.paisosCatalans.saldo >= 0){
        this.info += this.paisosCatalans.nom+" ha esclatat amb una tempesta elèctrica!";
        this.info2 += this.paisosCatalans.saldo+"\n";
        this.info2 += "-5000"+"\n";
        
        this.info2 += "-----------"+"\n";
        this.info2 += this.paisosCatalans.saldo;
      } else {
        this.paisosCatalans.activa = false;
        this.info += this.paisosCatalans.nom +" ha caigut per la tempesta!";
        this.cambiarMapa();
      }
      break; 
     }
   } 

  }

  cambiarMapa(){

    let g = this.euskalGaliza.activa;
    let i = this.illojuan.activa;
    let m = this.madrid.activa;
    let p = this.paisosCatalans.activa;

    if (g == false){
      if (i == true && m == true && p == true){
        this.mapa = "./assets/mapas/mapa-g.png";
      } else if (i == true && m == true && p == false){
        this.mapa = "./assets/mapas/mapa-gp.png";
      } else if (i == true && m == false && p == false){
        this.mapa = "./assets/mapas/mapa-gpm.png";
      } else if (i == true && m == false && p == true){
        this.mapa = "./assets/mapas/mapa-gm.png";
      } else if (i == false && m == false && p == true){
        this.mapa = "./assets/mapas/mapa-gim.png";
      } else if (i == false && m == true && p == true){
        this.mapa = "./assets/mapas/mapa-gi.png";
      }else if (i == false && m == true && p == false){
        this.mapa = "./assets/mapas/mapa-gpi.png";
      }
    } else if (i == false){
      if (m == true && p == true){
        this.mapa = "./assets/mapas/mapa-i.png";
      } else if (m == true && p == false){
        this.mapa = "./assets/mapas/mapa-ip.png";
      } else if (m == false && p == true){
        this.mapa = "./assets/mapas/mapa-mi.png";
      }
    } else if (m == false){
      if (p == true){
        this.mapa = "./assets/mapas/mapa-m.png";
      } else if (p == false){
        this.mapa = "./assets/mapas/mapa-mp.png";
      }
    } else if (p == false){
      this.mapa = "./assets/mapas/mapa-p.png";
    }

    if (g == true && m == false && p == false && i == false){
      this.mapa = "./assets/mapas/mapa-mpi.png";
    }
  }


}
