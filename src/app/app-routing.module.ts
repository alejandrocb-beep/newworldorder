import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'borsa',
    loadChildren: () => import('./borsa/borsa.module').then( m => m.BorsaPageModule)
  },
  {
    path: 'defensa',
    loadChildren: () => import('./defensa/defensa.module').then( m => m.DefensaPageModule)
  },
  {
    path: 'espai',
    loadChildren: () => import('./espai/espai.module').then( m => m.EspaiPageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then( m => m.AdminPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./modal/modal.module').then( m => m.ModalPageModule)
  },
  {
    path: 'fer-treball',
    loadChildren: () => import('./fer-treball/fer-treball.module').then( m => m.FerTreballPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
