import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefensaPage } from './defensa.page';

const routes: Routes = [
  {
    path: '',
    component: DefensaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DefensaPageRoutingModule {}
