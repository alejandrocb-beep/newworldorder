import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { users } from '../home/home.page';

import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { UsersService } from '../services/users.service';

interface Producte {
  nombre: string;
  descripcio: string;
  preu: number;
  quantitat: number;
  foto: string;
}



@Component({
  selector: 'app-defensa',
  templateUrl: './defensa.page.html',
  styleUrls: ['./defensa.page.scss'],
})
export class DefensaPage implements OnInit {



  logged = true;

  menuOFF = true;

  correu: string;

  glitched = false;

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  async showTheMenu(){
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    console.log("pickar"+this.menuOFF)
    if (this.menuOFF == true){
      this.menuOFF = false;
    } else {
      this.menuOFF = true;
    }
  }

  

  
  saldo: number;
  cargado = false;
  cargando = false;
  defc = false;
  conc = false;
  consumible = {};
  defensa = {};
  sub: any;
  nombre = "";
  rol: string;
  id: string;

  userData: users;
  productList = [];
  productData: Producte;
  productForm: FormGroup;


  constructor(
    private usersService: UsersService,
    private firebaseService: FirebaseService,
    private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder,
    public toastController: ToastController
  ) {

    this.productData = {} as Producte;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.nombre= this.router.getCurrentNavigation().extras.state.nom;
        this.saldo = this.router.getCurrentNavigation().extras.state.saldo;
        this.correu = this.router.getCurrentNavigation().extras.state.correu;
        this.id = this.router.getCurrentNavigation().extras.state.id;
        this.rol = this.router.getCurrentNavigation().extras.state.rol;
      }
    });
  }

  async goHome() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    if (this.logged){
      this.router.navigate(['./home']);
    } else {
      console.log("ACCESS DENIED to HOME!")
    }
  }

  userList = [];
  navigationExtras: NavigationExtras = { state: { } };

  async goBorsa() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    this.navigationExtras.state.nom = this.nombre;
    this.navigationExtras.state.saldo = this.saldo;
    this.navigationExtras.state.correu = this.correu;
    for (let entry of this.userList){
      console.log("mira todo")
      if (entry.correu === this.correu){
        this.navigationExtras.state.id = entry.id;
        this.navigationExtras.state.rol = entry.rol;
      }
    }
    if (this.logged){
      this.router.navigate(['./borsa'], this.navigationExtras);
    } else {
      console.log("ACCESS DENIED to BORSA!")
    }
  }

  async goEspai() {
    this.glitched = true;
    await this.delay(250);
    this.glitched = false;
    this.navigationExtras.state.nom = this.nombre;
    this.navigationExtras.state.saldo = this.saldo;
    this.navigationExtras.state.correu = this.correu;
    for (let entry of this.userList){
      console.log("mira todo")
      if (entry.correu === this.correu){
        this.navigationExtras.state.id = entry.id;
        this.navigationExtras.state.rol = entry.rol;
      }
    }
    if (this.logged){
      this.router.navigate(['./espai'], this.navigationExtras);
    } else {
      console.log("ACCESS DENIED to ESPAI!")
    }
  }




  async pagar(subir: string){
    if (this.cargado == true){
      this.cargado = false;
    }
    this.cargando = true;
    await this.delay(1000);
    let record = {};
    record['correu'] = this.correu;
    record['diners'] = this.saldo - parseInt(subir);
    record['nom'] = this.nombre;
    record['rol'] = "client";

    this.firebaseService.update(this.id,record,"users");
    this.saldo = this.saldo - parseInt(subir);
    this.cargando = false;
    this.cargado = true;
  }

  isReady = false;
  notLogged = true;
  ngOnInit() {
    this.usersService.read_user().subscribe(data => {

      this.userList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nom: e.payload.doc.data()['nom'],
          correu: e.payload.doc.data()['correu'],
          rol: e.payload.doc.data()['rol'],
          diners: e.payload.doc.data()['diners']
        };
      })
      console.log(this.userList);
      this.notLogged = false;
      if (this.nombre === ""){
        this.notLogged = true;
      }
      console.log(this.notLogged);
    });
    this.productForm = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcio: ['', [Validators.required]],
      preu: ['', [Validators.required]],
      quantitat: ['', [Validators.required]],
      foto: ['', [Validators.required]]
    })

    this.firebaseService.read("productes").subscribe(data => {
      this.productList = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          nombre: e.payload.doc.data()['nombre'],
          descripcio: e.payload.doc.data()['descripcio'],
          preu: e.payload.doc.data()['preu'],
          quantitat: e.payload.doc.data()['quantitat'],
          foto: e.payload.doc.data()['foto'],
          clicked: false,
          bought: false
        };
      })
      console.log(this.productList);
    
      this.defensa = this.productList[4];
      this.consumible = this.productList[7];
      console.log(this.defensa);
      console.log(this.consumible);
    });
  }

}
