import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DefensaPageRoutingModule } from './defensa-routing.module';

import { DefensaPage } from './defensa.page';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    DefensaPageRoutingModule
  ],
  declarations: [DefensaPage]
})
export class DefensaPageModule {}
